			<footer>
				<img src="<? image('footer-slant.svg') ?>" class="footer-slant">

				<div class="top-row">
					<div class="wrap animateelement fadein">
						<div class="column">
							<h3 class="h2">NEWSLETTER</h3>
							<p>Sign up to recieve our newsletter, once subscribed, you can opt-out at any time.</p>
							<div class="form-inner">
								<?= do_shortcode('[contact-form-7 id="106" title="Newsletter"]'); ?>
								<a class="custom-submit btn white-arrow"><span>Submit</span> <i class="fas fa-arrow-right"></i></a>
							</div>

						</div>
						<div class="column">
							<h3 class="h2">LINKS</h3>
							<nav role="navigation">
								<?php wp_nav_menu(array(
									'menu' => __( 'Footer Links', 'bonestheme' ),
									'theme_location' => 'footer-links',
								)); ?>
							</nav>
						</div>
						<div class="column">
							<h3 class="h2">CONNECT</h3>
							<a href="<? the_field('twitter_handle', 'options') ?>" class="twitter" target="_blank"><img src="<? image('footer-twitter.png') ?>" alt="Twitter Icon"></a>
						</div>
					</div>
				</div>

				<div class="bottom-row">
					<div class="wrap">
						<p class="source-org copyright">COPYRIGHT &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>. WEBSITE BY <a href="https://www.greensplashdesign.com/" target="_blank"><img src="<? image('greensplash.png') ?>" alt="Greensplash Web Design"></a></p>
					</div>
				</div>
			</footer>

			<div class="eu-strip animateelement fadein">
				<div class="wrap">
					<img src="<? the_field('eu_flag', 'options'); ?>" alt="EU Flag">
					<? the_field('eu_strip_content', 'options'); ?>
				</div>
			</div>

		<?php wp_footer(); ?>

	</body>
</html>
