<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?></title>
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
        <?php // drop Google Analytics Here ?>
        <?php // end analytics ?>

		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
		<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
		<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#c00000",
		      "text": "#ffffff"
		    },
		    "button": {
		      "background": "#003399",
		      "text": "#FFF"
		    }
		  },
			"theme": "classic",
			"position": "bottom",
			"content": {
				"message": "We use cookies to give you the best possible experience. By using our website, you're agreeing to our use of cookies. To find out more read our",
				"dismiss": "CLOSE",
				"link": "Cookie Policy",
				"href": "<? url() ?>/cookie-policy"
			}
		})});
		</script>


		<link rel="stylesheet" href="https://use.typekit.net/icv1oap.css">
        <script src="https://kit.fontawesome.com/d8ddbdf770.js"></script>
    </head>

	<body <?php body_class(); ?>>
		<header>
			<div class="wrap">
				<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><img src="<? image('logo.png') ?>" alt="<?php bloginfo( 'name' ); ?> Logo"></a>


					<nav>
						<div class="top-row">
							<a href="<? the_field('twitter_handle', 'options') ?>" class="twitter" target="_blank"><img src="<? image('twitter-icon.png') ?>" alt="Twitter Icon"></a>
							<a href="<? the_field('partner_login_url', 'options') ?>" class="btn pink"><span>Partner Login</span> <i class="fas fa-arrow-right"></i></a>
						</div>
						<?php wp_nav_menu(array(
							'menu' => __( 'The Main Menu', 'bonestheme' ),
							'theme_location' => 'main-nav',
						)); ?>
					</nav>

					<div class="menu-toggle">
						<div class="hamburger">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<div class="cross">
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
			</div>

			<img src="<? image('header-slant.svg') ?>" class="header-slant">
		</header>
