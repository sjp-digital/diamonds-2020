<? $date = get_the_date("d.m.Y") ?>
<? $thumb_id = get_post_thumbnail_id($post); ?>
<? $image_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

<?php get_header(); ?>

	<div class="banner">
		<div class="slide">
			<? if(get_field('banner_image', $post->ID)): ?>
				<? $image = get_field('banner_image', $post->ID); ?>
			<? else: ?>
				<?  $image =  get_field('default_banner_image', 'options'); ?>
			<? endif; ?>
			<div class="background-image" style="background: url('<?= $image_url[0]; ?>') center / cover;"></div>
			<?= wp_get_attachment_image( $image, 'full' ); ?>

			<div class="wrap">
				<div class="banner-content animateelement fadein">
					<h1><?= get_the_title($post->ID); ?>

					<div class="image-container">
						<img class="banner-accent" src="<? image('banner-accent.svg'); ?>" alt="Banner Accent">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="single-post-content">
		<div class="wrap">
			<div class="content animateelement fadein">
				<? the_content() ?>
			</div>

			<div class="sidebar animateelement fadein">
				<?= wp_get_attachment_image( $thumb_id, 'small-image' ); ?>

				<div class="sidebar-content">
					<h5>Date</h5>
					<p class="date"><?= $date; ?></p>
					<h5>Share This</h5>
					<?php social_share(array('f', 't', 'l'), $post); ?>

					<a href="<? url('news'); ?>" class="btn white-arrow dark-blue"><span>View all news <i class="fas fa-arrow-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>

	<? if( have_rows('flexible_content', $post->ID) ) : ?>
		<div class="single-flexible-blocks">
			<? include 'flexible-content.php'; ?>
		</div>
	<? endif; ?>

<? get_footer(); ?>
