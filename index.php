<?php get_header(); ?>

	<div class="banner">
		<? while ( have_rows('banner_slides', get_option('page_for_posts')) ) : the_row(); ?>
			<div class="slide">
				<? $image = get_sub_field('banner_image', get_option('page_for_posts')); ?>
				<?= wp_get_attachment_image( $image, 'full' ); ?>

				<div class="wrap">
					<? $link = get_sub_field('button', get_option('page_for_posts')); ?>

					<? if($link): ?>
						<a class="banner-content banner-button animateelement fadein" href="<? the_sub_field('button_link', get_option('page_for_posts')); ?>">
							<? the_sub_field('banner_content', get_option('page_for_posts')); ?>

							<div class="image-container">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="86.5px" height="150px" viewBox="0 0 86.5 150" style="enable-background:new 0 0 86.5 150;" xml:space="preserve">
									<style type="text/css">
										.st0{fill:#C00000;}
										.st1{fill:#FFFFFF;}
										.st2{fill:#003399;}
									</style>
									<g class="arrow">
										<polygon class="st0" points="51.6,104 16.5,34 86.5,34.5 	"/>
										<g>
											<path class="st1" d="M51.7,49.8l9.5,9.5c0.2,0.2,0.2,0.6,0,0.8l-9.5,9.5c-0.2,0.2-0.6,0.2-0.8,0l-0.9-0.9c-0.2-0.2-0.2-0.6,0-0.8l7.1-7.1H41.6c-0.3,0-0.5-0.2-0.5-0.5v-1.3c0-0.3,0.2-0.5,0.5-0.5h15.5l-7.1-7.1c-0.2-0.2-0.2-0.6,0-0.8l0.9-0.9C51.2,49.6,51.5,49.6,51.7,49.8z"/>
										</g>
										<polyline class="st0" points="16.6,34 0,34 0,104 51.6,104 	"/>
									</g>
									<polygon class="st2" points="0,0 75,150 0,150 "/>
								</svg>
							</div>
						</a>
					<? else: ?>
						<div class="banner-content animateelement fadein">
							<? the_sub_field('banner_content', get_option('page_for_posts')); ?>

							<div class="image-container">
								<img class="banner-accent" src="<? image('banner-accent.svg'); ?>" alt="Banner Accent">
							</div>
						</div>
					<? endif; ?>
				</div>
			</div>
		<? endwhile; ?>
	</div>

	<div class="news-overview wrap">
		<div class="animateelement fadein"><? the_field('introductory_content', get_option('page_for_posts')); ?></div>

		<div class="post-grid">
			<?php while (have_posts()) : the_post(); ?>
				<?  $thumb_id = get_post_thumbnail_id($post); ?>
				<? $date = get_the_date("d.m.Y") ?>

				<div class="post animateelement fadein">
					<a href="<?= get_the_permalink($post->ID) ?>">
						<?= wp_get_attachment_image( $thumb_id, 'small-image' ); ?>
						<? $excerpt = get_the_content($post->ID); ?>

						<div class="content">
							<h5><?= get_the_title($post->ID) ?></h5>
							<p class="date"><?= $date; ?></p>
							<p class="excerpt"><?= wp_trim_words($excerpt, 30); ?></p>
							<div class="btn white-arrow dark-blue"><span>Read more</span> <i class="fas fa-arrow-right"></i></div>
						</div>
					</a>
				</div>
			<?php endwhile; ?>
		</div>

		<?php bones_page_navi(); ?>

	</div>

<?php get_footer(); ?>
