<div class="partners-carousel animateelement fadein">
	<div class="wrap">
		<h2 class="h1">OUR PARTNERS</h2>

		<? $images = get_sub_field('partners', $post->ID);
		$size = 'small-image'; ?>

		<div class="partner-logos">
			<? foreach( $images as $image ): ?>
				<div class="item">
					<?= wp_get_attachment_image( $image, $size ); ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
