<? $image = get_sub_field('feature_image', $post->ID); ?>
<? $btn = get_sub_field('feature_link', $post->ID); ?>

<div class="text-w-sidebar">
	<div class="wrap">
		<div class="content animateelement fadein">
			<? the_sub_field('text_w_sidebar_content', $post->ID); ?>
		</div>

		<div class="sidebar animateelement fadein">
			<?= wp_get_attachment_image( $image, 'small-image' ); ?>

			<? if($btn): ?>
				<a href="<?php echo $btn['url']; ?>" class="btn"><span><?php echo $btn['title']; ?></span> <i class="fas fa-arrow-right"></i></a>
			<? endif; ?>
		</div>
	</div>
</div>



