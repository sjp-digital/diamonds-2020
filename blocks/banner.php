<div class="banner">
	<? while ( have_rows('banner_slides', $post->ID) ) : the_row(); ?>
		<? $image = get_sub_field('banner_image', $post->ID); ?>
		<? $image_url = wp_get_attachment_image_src($image, 'full'); ?>

		<div class="slide">
			<div class="background-image" style="background: url('<?= $image_url[0]; ?>') center / cover;"></div>
			<?= wp_get_attachment_image( $image, 'full' ); ?>

			<div class="wrap">
				<? $link = get_sub_field('button', $post->ID); ?>

				<? if($link): ?>
					<a class="banner-content banner-button animateelement fadein" href="<? the_sub_field('button_link', $post->ID); ?>">
						<? the_sub_field('banner_content', $post->ID); ?>

						<div class="image-container">
							<img class="banner-accent" src="<? image('banner-button-indicator.svg'); ?>" alt="Banner Accent">
						</div>
					</a>
				<? else: ?>
					<div class="banner-content animateelement fadein">
						<? the_sub_field('banner_content', $post->ID); ?>

						<div class="image-container">
							<img class="banner-accent" src="<? image('banner-accent.svg'); ?>" alt="Banner Accent">
						</div>
					</div>
				<? endif; ?>
			</div>
		</div>
	<? endwhile; ?>
</div>
