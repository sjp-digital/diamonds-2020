<? $image = get_sub_field('full_width_image', $post->ID); ?>

<div class="full-width-image">
	<div class="wrap">
		<h2 class="h1 animateelement fadein"><? the_sub_field('title', $post->ID) ?></h2>
		<div class="image-container animateelement fadein">
			<?= wp_get_attachment_image( $image, 'ipad-image' ); ?>
		</div>
	</div>
</div>
