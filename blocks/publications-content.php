<? $image = get_sub_field('feature_image', $post->ID); ?>
<? $btn = get_sub_field('feature_link', $post->ID); ?>

<div class="publications-content">
	<div class="wrap">
		<div class="content">
			<div class="animateelement fadein"><? the_sub_field('introductory_content', $post->ID); ?></div>


			<? if( have_rows('publications', $post->ID) ):
				while ( have_rows('publications', $post->ID) ) : the_row(); ?>
					<div class="publication animateelement fadein">
						<p><strong><? the_sub_field('authors', $post->ID); ?></strong></p>
						<p><? the_sub_field('description', $post->ID); ?></p>
						<p><em><? the_sub_field('reference', $post->ID); ?></em></p>

						<? if(get_sub_field('publication_format', $post->ID) == 'file'): ?>
							<a target="_blank" href="<? the_sub_field('file_upload', $post->ID); ?>" class="btn white-arrow dark-blue"><span>View publication</span> <i class="fas fa-arrow-right"></i></a>
						<? else: ?>
							<a target="_blank" href="<? the_sub_field('pdf_url', $post->ID); ?>" class="btn white-arrow dark-blue"><span>View publication</span> <i class="fas fa-arrow-right"></i></a>
						<? endif; ?>
					</div>
				<? endwhile;
			endif; ?>

		</div>

		<div class="sidebar animateelement fadein">
			<?= wp_get_attachment_image( $image, 'small-image' ); ?>

			<? if($btn): ?>
				<a href="<?php echo $btn['url']; ?>" class="btn"><span><?php echo $btn['title']; ?></span> <i class="fas fa-arrow-right"></i></a>
			<? endif; ?>
		</div>
	</div>
</div>



