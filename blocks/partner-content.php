<? $introductory_content = get_sub_field('introductory_content', $post->ID);
$roles_and_responsibilities = get_sub_field('roles_and_responsibilities', $post->ID);
$clinic_lab_profile = get_sub_field('clinic_lab_profile', $post->ID);
$additional_content = get_sub_field('additional_content', $post->ID);

$thumb_id = get_post_thumbnail_id($post);
$contact_person = get_sub_field('contact_person', $post->ID);
$contact_role = get_sub_field('contact_role', $post->ID);
$contact_email = get_sub_field('contact_email', $post->ID);
$address = get_sub_field('address', $post->ID);
$website_url = get_sub_field('website_url', $post->ID); ?>


<div class="partner-content">
	<div class="wrap">
		<div class="content animateelement fadein">
			<? if($introductory_content): ?>
				<?= $introductory_content ; ?>
			<? endif; ?>

			<? if($roles_and_responsibilities): ?>
				<h2>ROLES AND RESPONSIBILITIES</h2>
				<?= $roles_and_responsibilities ; ?>
			<? endif; ?>

			<? if($clinic_lab_profile): ?>
				<h3>CLINIC/ LAB PROFILE</h3>
				<?= $clinic_lab_profile ; ?>
			<? endif; ?>

			<? if($additional_content): ?>
				<?= $additional_content ; ?>
			<? endif; ?>

			<a href="<? url('partners'); ?>" class="btn"><span>See all Partners <i class="fas fa-arrow-right"></i></span></a>
		</div>
		<div class="sidebar animateelement fadein">
			<div class="image-container">
				<?= wp_get_attachment_image( $thumb_id, 'small-image' ); ?>
			</div>

			<div class="sidebar-content">

			<? if($contact_person): ?>
				<h5>CONTACT PERSON</h5>
				<p><?= $contact_person ; ?></p>
			<? endif; ?>

			<? if($contact_role): ?>
				<h5>CONTACT ROLE</h5>
				<p><?= $contact_role ; ?></p>
			<? endif; ?>

			<? if($contact_email): ?>
				<h5>CONTACT EMAIL</h5>
				<p><a class="email" href="mailto:<?= $contact_email ; ?>"><?= $contact_email ; ?></a></p>
			<? endif; ?>

			<? if($address): ?>
				<h5>ADDRESS</h5>
				<p><?= $address ; ?></p>
			<? endif; ?>

			<? if($website_url): ?>
				<a class="btn white-arrow dark-blue" href="<?= $website_url ; ?>"><span>Website Link</span> <i class="fas fa-arrow-right"></i></a>
			<? endif; ?>
			</div>
		</div>
	</div>
</div>

