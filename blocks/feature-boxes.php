<div class="feature-boxes">
	<h2 class="h1 animateelement fadein"><? the_sub_field('title', $post->ID); ?></h2>

	<div class="wrap">
		<? while ( have_rows('feature_boxes') ) : the_row(); ?>
			<? $image = get_sub_field('image', $post->ID); ?>
			<? $btn = get_sub_field('link', $post->ID); ?>

			<div class="feature-box animateelement fadein">
				<?= wp_get_attachment_image( $image, 'small-image' ); ?>

				<? if($btn): ?>
					<a href="<?php echo $btn['url']; ?>" class="btn"><span><?php echo $btn['title']; ?></span> <i class="fas fa-arrow-right"></i></a>
				<? endif; ?>
			</div>
		<? endwhile; ?>
	</div>
</div>
