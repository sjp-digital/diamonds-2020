<div class="contact-block">
	<div class="wrap">
		<div class="animateelement fadein">
			<? the_sub_field('introductory_content', $post->ID); ?>
		</div>

		<div class="form-container animateelement fadein">
			<div class="form-inner">
				<?= do_shortcode('[contact-form-7 id="5" title="Main Form"]'); ?>
				<a class="custom-submit btn"><span>Submit</span> <i class="fas fa-arrow-right"></i></a>
			</div>

			<div class="sidebar animateelement fadein">
				<h3>DIAMONDS</h3>
				<p><? the_field('address', 'options'); ?></p>

				<h3>Contact</h3>
				<p><a href="<? the_field('email_address', 'options'); ?>"><? the_field('email_address', 'options'); ?></a></p>

				<h3>Connect</h3>
				<a href="<? the_field('twitter_handle', 'options') ?>" class="twitter" target="_blank"><img src="<? image('footer-twitter.png') ?>" alt="Twitter Icon"></a>
			</div>
		</div>
	</div>
</div>
