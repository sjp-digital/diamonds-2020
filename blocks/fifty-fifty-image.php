<? $image = get_sub_field('fifty_fifty_image', $post->ID); ?>
<? $btn = get_sub_field('link', $post->ID); ?>

<div class="fifty-fifty-image">
	<div class="wrap">
		<div class="image-wrap animateelement fadein">
			<?= wp_get_attachment_image( $image, 'ipad-image' ); ?>
		</div>

		<div class="content animateelement fadein">
			<? the_sub_field('fifty_fifty_text_content', $post->ID); ?>

			<? if($btn): ?>
				<a href="<?php echo $btn['url']; ?>" class="btn"><span><?php echo $btn['title']; ?></span> <i class="fas fa-arrow-right"></i></a>
			<? endif; ?>
		</div>
	</div>
</div>






