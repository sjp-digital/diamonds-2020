<?php
require_once('TwitterAPIExchange.php');
/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
'oauth_access_token' => "1222800234759278596-9dxDofVGHyszrBPbpMDqbtb2cxXHS2",
'oauth_access_token_secret' => "wb6MKLAZ0Ash7kxUU5ssz8XBzj5dBLgSlkPaGLE0e45C5",
'consumer_key' => "X6THN1SqhIvEK0P5xdwKl1d0p",
'consumer_secret' => "03LAYLTIAB8RgnQ4vCOUicZ34ueW6LwOXnQXtYCR9Rbw2E8hqr"
);

if(get_field('twitter_handle', 'options')):
    $user = get_field('twitter_handle', 'option');
else:
    $user = 'greensplashltd';
endif;

$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
$requestMethod = "GET";
if (isset($_GET['count']) && is_numeric($_GET['count'])) {$count = $_GET['count'];} else {$count = 1;}
$getfield = "?screen_name=$user&count=$count&tweet_mode=extended";
$twitter = new TwitterAPIExchange($settings);
$string = json_decode($twitter->setGetfield($getfield)
->buildOauth($url, $requestMethod)
->performRequest(),$assoc = TRUE); ?>


<div class="news-strip">
	<div class="wrap">
		<div class="twitter-feed animateelement fadein">
			<h3>LATEST TWEET</h3>
			<? if(array_key_exists("errors", $string)) {echo "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following error message:</p><p><em>".$string[errors][0]["message"]."</em></p>";exit();}
	            foreach($string as $items): ?>
	                <? if (isset($items['retweeted_status'])): ?>
	                   <? $text = $items["retweeted_status"]["full_text"]  ?>
	                <? else: ?>
	                    <? $text = $items['full_text']; ?>
	                 <? endif; ?>

	                <? $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
	                $text = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $text);
	                $text = preg_replace('/(?<!\S)#([0-9a-zA-Z]+)/', '<a href="http://twitter.com/hashtag/$1" target="_blank">#$1</a>', $text);
	                $text = preg_replace('/(?<!\S)@([0-9a-zA-Z]+)/', '<a href="http://twitter.com/$1" target="_blank">@$1</a>', $text); ?>

	                <? $date = date_create($items['created_at']); ?>
	                <? $date = date_format($date,"dS F Y"); ?>

	                <div class="tweet">
	                    <p><em><?= $text; ?></em></p>
	                    <p><?= $date; ?></p>
	                    <div class="tweet-actions">
	                    	<a class="tweet-action" href="https://twitter.com/intent/tweet?in_reply_to=<?= $items['id_str'] ?>"><i class="fas fa-reply"></i></a>
	                   	 	<a class="tweet-action" href="https://twitter.com/intent/retweet?tweet_id=<?= $items['id_str'] ?>"><i class="fas fa-retweet"></i></a>
	                    	<a class="tweet-action" href="https://twitter.com/intent/favorite?tweet_id=<?= $items['id_str'] ?>"><i class="fas fa-heart"></i></a>
	                    </div>
	                </div>
	            <? endforeach; ?>
	           </div>

		<div class="latest-news animateelement fadein">
			<h3>LATEST NEWS</h3>

			<? latest_news(); ?>
		</div>
	</div>
</div>

