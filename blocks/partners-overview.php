<div class="partners-overview">
	<div class="wrap">
		<div class="animateelement fadein"><? the_sub_field('introductory_text', $post->ID); ?></div>

		<div class="partners-grid">
			<? partners_grid(); ?>
		</div>
	</div>
</div>
