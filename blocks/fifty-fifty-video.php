<? $btn = get_sub_field('link', $post->ID); ?>

<div class="fifty-fifty-video">
	<div class="wrap">
		<div class="video-container animateelement fadein">
			<? the_sub_field('fifty_fifty_video', $post->ID); ?>
		</div>

		<div class="content">
			<? the_sub_field('fifty_fifty_text_content', $post->ID); ?>

			<? if($btn): ?>
				<a href="<?php echo $btn['url']; ?>" class="btn"><span><?php echo $btn['title']; ?></span> <i class="fas fa-arrow-right"></i></a>
			<? endif; ?>
		</div>
	</div>
</div>






