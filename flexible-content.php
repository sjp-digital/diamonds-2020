	<? while ( have_rows('flexible_content', $post->ID) ) : the_row();

		if( get_row_layout() == 'banner' ):

			include block('banner.php');

		elseif( get_row_layout() == 'text_w_sidebar' ):

			include block('text-w-sidebar.php');

		elseif( get_row_layout() == 'full_width_text_column' ):

			include block('full-width-text.php');

		elseif( get_row_layout() == 'news_strip' ):

			include block('news-strip.php');

		elseif( get_row_layout() == 'partners_carousel' ):

			include block('partners-carousel.php');

		elseif( get_row_layout() == 'fifty_fifty_text' ):

			include block('fifty-fifty-image.php');

		elseif( get_row_layout() == 'fifty_fifty_video' ):

			include block('fifty-fifty-video.php');

		elseif( get_row_layout() == 'partners_overview' ):

			include block('partners-overview.php');

		elseif( get_row_layout() == 'partner_content' ):

			include block('partner-content.php');

		elseif( get_row_layout() == 'publications_content' ):

			include block('publications-content.php');

		elseif( get_row_layout() == 'contact_block' ):

			include block('contact-block.php');

		elseif( get_row_layout() == 'full_width_image' ):

			include block('full-width-image.php');

		elseif( get_row_layout() == 'feature_boxes' ):

			include block('feature-boxes.php');

		endif;

	endwhile; ?>
