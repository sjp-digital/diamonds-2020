jQuery(document).ready(function($) {
	var windowWidth = $(window).width()

/* ====================== MOBILE NAV ======================  */
    $('.menu-toggle').on("click", function() {
        $(this).toggleClass('active');
        $('header nav').toggleClass('open');
        $('body').toggleClass('noscroll')
    })

/* ====================== HP SLIDER ======================  */
	$('.banner').slick({
		fade: true,
  		cssEase: 'linear',
  		speed: 1000,
		slidesToShow: 1,
  		slidesToScroll: 1,
		dots: true,
		arrows: false,
		autoplay: true,
	});

/* ====================== TWEET ACTIONS ======================  */
    $('.tweet-action').on("click", function() {
    	javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;
    })

/* ====================== PARTNERS CAROUSEL ======================  */
	$('.partner-logos').slick({
  		speed: 2000,
		slidesToShow: 5,
  		slidesToScroll: 5,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		      	speed: 1500,
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
	});

/* ====================== TRIGGER FORM SUBMIT ======================  */
	$('.wpcf7-acceptance input').change(function () {
		if ($(this).is(':checked')) {
			$(this).parents('.form-inner').find('.custom-submit').removeClass("disabled");
			$(this).parent('label').toggleClass('checked');
		} else {
			$(this).parents('.form-inner').find('.custom-submit').addClass("disabled");
		}
	}).change()

	$(".custom-submit").click(function(){
		$(this).parents('.form-inner').find('.wpcf7-submit').trigger( "click" );
    });

/* ====================== ANIMATE ELEMENTS ======================  */
	$('.animateelement').each(function(){
		$(this).waypoint({
			handler: function(direction){
				$(this.element).addClass('animate');
			},
		   offset: '90%'
	   });
	})

})
