<?php
# =========== STYLE LOGIN =========== #
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/dist/login-style.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

add_filter( 'login_headerurl', 'codecanal_loginlogo_url' );
function codecanal_loginlogo_url($url)
{
  return home_url();
}

# =========== LATEST NEWS =========== #
	function latest_news() {
		$args = array(
			'post_status'    	=> 'publish',
			'post_type'        	=> 'post',
			'posts_per_page'	=> '1',
		);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>
				<div class="latest-post">
					<?  $thumb_id = get_post_thumbnail_id($post); ?>
					<? $date = get_the_date("dS F Y") ?>

					<?= wp_get_attachment_image( $thumb_id, 'iphone' ); ?>

					<div class="content">
						<h5><?= get_the_title($post->ID) ?></h5>
						<p><?= $date; ?></p>
						<a href="<?= get_the_permalink($post->ID) ?>" class="btn white-arrow"><span>Read more</span> <i class="fas fa-arrow-right"></i></a>
					</div>
				</div>
		<?php endforeach;

		wp_reset_query();
	}

# =========== PARTNERS GRID =========== #
	function partners_grid() {
		$args = array(
			'post_status'    	=> 'publish',
			'post_type'        	=> 'partners',
			'posts_per_page'	=> '-1',
		);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>
			<?  $thumb_id = get_post_thumbnail_id($post); ?>

			<div class="partner animateelement fadein">
				<a href="<?= get_the_permalink($post->ID) ?>">
					<div class="image-container">
						<?= wp_get_attachment_image( $thumb_id, 'iphone' ); ?>
					</div>

					<div class="content">
						<h5><?= get_the_title($post->ID) ?></h5>
						<div class="btn white-arrow dark-blue"><span>Read more</span> <i class="fas fa-arrow-right"></i></div>
					</div>
				</a>
			</div>
		<?php endforeach;

		wp_reset_query();
	}


# =========== SOCIAL SHARE GENERATOR =========== #
	function social_share($types, $post) {
	$url = urlencode(get_permalink($post->ID));
	$title = urlencode($post->post_title);
	$description = urlencode(get_the_excerpt($post));
	$image = get_the_post_thumbnail_url($post->ID, 'full');
	$js = "onclick='javascript:window.open(this.href,\" \", \"menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\"";

	?>

	<div class="social-share">
		<?php
			foreach($types as $t):
				switch($t):
					case 'f': ?>
						<a class="social-share-link facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=$url;?>&amp;title=<?=$title;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fab fa-facebook-f"></i></a>
					<?php break;
					case 't'; ?>
						<a class="social-share-link twitter" href="http://twitter.com/intent/tweet?status=<?=$description." ".$url;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fab fa-twitter"></i></a>
					<?php break;
					case 'p'; ?>
						<a class="social-share-link social-share-link--pinterest" href="https://pinterest.com/pin/create/button/?url=<?=$url;?>&media=<?=$image;?>&description=<?=$description;?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=800,width=900');return false;"><i class="fab fa-pinterest"></i></a>
					<?php break;

					case 'g'; ?>
						<a class="social-share-link social-share-link--googleplus" href="https://plus.google.com/share?url=<?=$url;?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fab fa-google-plus-g"></i></a>
					<?php break;

					case 'l'; ?>
						<a class="social-share-link linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$url;?>&summary=<?=$description;?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=800,width=900');return false;"><i class="fab fa-linkedin-in"></i></a>
					<?php break;

				endswitch;
			endforeach;
		?>
	</div> <?php
	}


# =========== ADD ACF OPTIONS =========== #
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

# =========== REMOVE AUTI P CF7 =========== #
    add_filter('wpcf7_autop_or_not', '__return_false');

# =========== REMOVE EMOJIS =========== #
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

# =========== MOVE YOAST TO BOTTOM =========== #
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
